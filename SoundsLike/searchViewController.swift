//
//  searchViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/3/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class searchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    let searchResults: NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.scopeButtonTitles = ["Band", "Musician"]
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = UIColor.init(red: 14/255, green: 86/255, blue: 178/255, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchResults.removeAllObjects()
        self.tableView.reloadData()
        
        if (searchText != "")
        {
            var query = PFQuery()
        
            if (searchBar.selectedScopeButtonIndex == 0) {
                query = PFQuery(className: "band")
            }
        
            if (searchBar.selectedScopeButtonIndex == 1) {
                query = PFQuery(className: "musician")
            }
        
            query.whereKey("searchName", contains: searchBar.text?.lowercased())
            query.limit = 100
        
            query.findObjectsInBackground {
                (objects, error) -> Void in
                if let objects = objects {
                    for object in objects {
                        self.searchResults.add(object)
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "moveToBandFromSearch", sender: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let bvc: bandViewController = segue.destination as! bandViewController
        let ip = sender as! IndexPath
        bvc.band = searchResults[ip.row] as? PFObject
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchTableViewCell", for: indexPath) as! searchTableViewCell
        let object = searchResults[indexPath.row] as! PFObject
        cell.name.text = object["name"] as? String
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

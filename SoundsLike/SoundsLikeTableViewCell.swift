//
//  SoundsLikeTableViewCell.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/4/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class SoundsLikeTableViewCell: UITableViewCell {

    @IBOutlet var bandName: UILabel!
    @IBOutlet var count: UILabel!
    @IBOutlet var likeSoundsLike: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

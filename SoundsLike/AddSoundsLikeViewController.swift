//
//  AddSoundsLikeViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/4/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class AddSoundsLikeViewController: UIViewController {
    
    var band: PFObject? = nil
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var nameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = band?["name"] as! String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveBtnTouched(_ sender: Any) {
        let soundsLike = PFObject(className: "soundsLike")
        soundsLike["band"] = band
        soundsLike["name"] = nameTextField.text
        soundsLike["searchName"] = nameTextField.text?.lowercased()
        soundsLike["count"] = "1"
        soundsLike["users"] = [PFUser.current()?["username"] as? String]
        soundsLike["createdBy"] = PFUser.current()?["username"] as? String
        
        soundsLike.saveInBackground {
            (success, error) -> Void in
            if (success) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

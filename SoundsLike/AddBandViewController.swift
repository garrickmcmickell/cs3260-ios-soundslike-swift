//
//  AddBandViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/1/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class AddBandViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var name: UITextField!
    @IBOutlet var loading: UIActivityIndicatorView!
    @IBOutlet var heightTVGenre: NSLayoutConstraint!
    @IBOutlet var genreTableView: UITableView!
    @IBOutlet var heightTVMusician: NSLayoutConstraint!
    @IBOutlet var memberTableView: UITableView!
    var musicians: NSMutableArray = []
    var genres: NSMutableArray = []
    var currBand: PFObject? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        heightTVMusician.constant = CGFloat(musicians.count) * 44
        heightTVGenre.constant = CGFloat(genres.count) * 44
        view.layoutIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addMusicianBtnTouched(_ sender: Any) {
        musicians.add("")
        memberTableView.reloadData()
        
        heightTVMusician.constant = CGFloat(musicians.count) * 44
        view.layoutIfNeeded()
    }

    @IBAction func addGenreBtnTouched(_ sender: Any) {
        genres.add("")
        genreTableView.reloadData()
        
        heightTVGenre.constant = CGFloat(genres.count) * 44
        view.layoutIfNeeded()
    }

    @IBAction func memberNameChanged(_ sender: Any) {
        var indexPath: NSIndexPath!
        
        if let textField = sender as? UITextField {
            if let superview = textField.superview {
                if let cell = superview.superview as? AddBandMusicianTableViewCell {
                    indexPath = memberTableView.indexPath(for: cell) as NSIndexPath!
                    musicians[indexPath.row] = cell.nameTextField.text!
                }
            }
        }
    }
    
    @IBAction func genreNameChanged(_ sender: Any) {
        var indexPath: NSIndexPath!
        
        if let textField = sender as? UITextField {
            if let superview = textField.superview {
                if let cell = superview.superview as? AddBandGenreTableViewCell {
                    indexPath = genreTableView.indexPath(for: cell) as NSIndexPath!
                    genres[indexPath.row] = cell.genreTextField.text!
                }
            }
        }
    }
    
    @IBAction func saveBtnTouched(_ sender: Any) {
        let band = PFObject(className: "band")
        band["name"] = name.text
        band["searchName"] = name.text?.lowercased()
        band["createdBy"] = PFUser.current()
        loading.startAnimating()
        view.alpha = 0.5
        
        band.saveInBackground {
            (success, error) -> Void in
            if(success) {
                for i in 0..<self.musicians.count{
                    let musician = PFObject(className: "musician")
                    musician["band"] = band
                    musician["name"] = self.musicians[i]
                    musician["createdBy"] = PFUser.current()
                    musician.saveInBackground {
                        (success1, error1) -> Void in
                        if (error1 != nil) {
                            let alert = UIAlertController(title: "Error", message: error1?.localizedDescription ?? String(), preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                
                for i in 0..<self.genres.count{
                    let genre = PFObject(className: "genre")
                    genre["band"] = band
                    genre["genre"] = self.genres[i]
                    genre["createdBy"] = PFUser.current()
                    genre.saveInBackground {
                        (success1, error1) -> Void in
                        if (error1 != nil) {
                            let alert = UIAlertController(title: "Error", message: error1?.localizedDescription ?? String(), preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                
                self.loading.stopAnimating()
                self.view.alpha = 1
                self.currBand = band
                self.performSegue(withIdentifier: "moveToBand", sender: nil)
            }
            else {
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription ?? String(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }        
    }
    
    @IBAction func musicianDeleteBtnTouched(_ sender: Any) {
        var indexPath: NSIndexPath!
        
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? AddBandMusicianTableViewCell {
                    indexPath = memberTableView.indexPath(for: cell) as NSIndexPath!
                }
            }
        }
        
        musicians.removeObject(at: indexPath.row)
        memberTableView.reloadData()
        heightTVMusician.constant = CGFloat(musicians.count) * 44
    }
    
    @IBAction func genreDeleteBtnTouched(_ sender: Any) {
        var indexPath: NSIndexPath!
        
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? AddBandGenreTableViewCell {
                    indexPath = genreTableView.indexPath(for: cell) as NSIndexPath!
                }
            }
        }
        
        genres.removeObject(at: indexPath.row)
        genreTableView.reloadData()
        heightTVGenre.constant = CGFloat(genres.count) * 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tableView == self.memberTableView) {
            let cell = memberTableView.dequeueReusableCell(withIdentifier: "AddBandMusicianTableViewCell", for: indexPath) as! AddBandMusicianTableViewCell
            cell.nameTextField.text = musicians[indexPath.row] as? String
            return cell
        }
        
        if (tableView == self.genreTableView) {
            let cell = genreTableView.dequeueReusableCell(withIdentifier: "AddBandGenreTableViewCell", for: indexPath) as! AddBandGenreTableViewCell
            cell.genreTextField.text = genres[indexPath.row] as? String
            return cell
        }
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == self.memberTableView) {
            return musicians.count
        }
        
        if (tableView == self.genreTableView) {
            return genres.count
        }
        
        return 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "moveToBand") {
            let bvc: bandViewController = segue.destination as! bandViewController
            bvc.band = currBand!
        }
    }
}

//
//  EditProfileViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class EditProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var heightTVinfo: NSLayoutConstraint!
    
    var info: NSMutableArray = []
    
    struct field {
        var type = String()
        var value = String()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        var email = field()
        email.type = "Email"
        email.value = PFUser.current()?.object(forKey: "email") as! String
        
        var password = field()
        password.type = "Password"
        password.value = ""
        
        info.add(email)
        info.add(password)
        
        heightTVinfo.constant = CGFloat(info.count) * 44
    }

    @IBAction func saveBtnTouched(_ sender: Any) {
        let email = self.info[0] as! field
        let password = self.info[1] as! field
        
        PFUser.current()?.email = email.value
        
        if (password.value != "") {
            PFUser.current()?.password = password.value
        }
        
        PFUser.current()?.saveInBackground {
            (success, error) -> Void in
            if (success) {
                self.navigationController?.popViewController(animated: true)
            }
            else {
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription ?? String(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
        
    }

    @IBAction func cancelBtnTouched(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileTableViewCell", for: indexPath) as! EditProfileTableViewCell
        let object = info[indexPath.row] as! field
        cell.type.text = object.type
        cell.value.placeholder = object.type
        cell.value.text = object.value
        
        if (object.type == "Password") {
            cell.value.isSecureTextEntry = true
        }
        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  SignupViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 11/19/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class SignupViewController: UIViewController {

    @IBOutlet var signUp: UIButton!
    @IBOutlet var password: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var username: UITextField!
    @IBOutlet var tabBar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        signUp.layer.borderColor = UIColor.init(red: 103/255, green: 193/255, blue: 166/255, alpha: 1).cgColor
        signUp.layer.borderWidth = 1
        signUp.layer.cornerRadius = 5

        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SignUpBtnTouched(_ sender: Any) {
        let user = PFUser()
        user.username = username.text
        user.email = email.text
        user.password = password.text
        
        let imageData = UIImagePNGRepresentation(#imageLiteral(resourceName: "defaultProfile.png"))
        user["image"] = PFFile(name: "defaultProfile.png", data: imageData!)
        
        user.signUpInBackground { (success, error) in
            if (success) {
                self.loginUser()
            }
            else {
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription ?? String(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func loginUser() -> Void {
        PFUser.logInWithUsername(inBackground: username.text!, password: password.text!, block: { (success, error) in
            if (success != nil) {
                self.performSegue(withIdentifier: "moveToProfile", sender: nil)
            }
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

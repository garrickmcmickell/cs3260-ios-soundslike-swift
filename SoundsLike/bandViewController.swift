//
//  bandViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/3/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class bandViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var heightTVgenre: NSLayoutConstraint!
    @IBOutlet var heightTVmusician: NSLayoutConstraint!
    @IBOutlet var heightTVsoundsLike: NSLayoutConstraint!
    @IBOutlet var soundsLikeTableView: UITableView!
    @IBOutlet var genreTableView: UITableView!
    @IBOutlet var musicianTableView: UITableView!
    var band: PFObject? = nil
    var soundsLike: NSMutableArray = []
    var musicians: NSMutableArray = []
    var genres: NSMutableArray = []
    @IBOutlet var name: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        soundsLike.removeAllObjects()
        musicians.removeAllObjects()
        genres.removeAllObjects()
        
        name.text = band?["name"] as! String?
        
        let query = PFQuery(className: "musician")
        query.whereKey("band", equalTo: band!)
        query.limit = 5
        query.findObjectsInBackground {
            (objects, error) -> Void in
            if let objects = objects {
                for object in objects {
                    self.musicians.add(object)
                }
            }
            self.heightTVmusician.constant = CGFloat(self.musicians.count) * 44
            self.musicianTableView.reloadData()
        }
        
        let query1 = PFQuery(className: "genre")
        query1.whereKey("band", equalTo: band!)
        query1.limit = 5
        query1.findObjectsInBackground {
            (objects, error) -> Void in
            if let objects = objects {
                for object in objects {
                    self.genres.add(object)
                }
            }
            self.heightTVgenre.constant = CGFloat(self.genres.count) * 44
            self.genreTableView.reloadData()
        }
        
        let query2 = PFQuery(className: "soundsLike")
        query2.whereKey("band", equalTo: band!)
        query2.addDescendingOrder("count")
        query2.limit = 5
        
        query2.findObjectsInBackground {
            (objects, error) -> Void in
            if let objects = objects {
                for object in objects {
                    self.soundsLike.add(object)
                }
            }
            self.heightTVsoundsLike.constant = CGFloat(self.soundsLike.count) * 44
            self.soundsLikeTableView.reloadData()
        }
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == self.musicianTableView) {
            return musicians.count
        }
        
        if (tableView == self.genreTableView) {
            return genres.count
        }
        
        if (tableView == self.soundsLikeTableView) {
            return soundsLike.count
        }
        
        return 0
    }
    
    @IBAction func likeSoundsLikeBtnTouched(_ sender: Any) {
        var indexPath: NSIndexPath!
        
        
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? SoundsLikeTableViewCell {
                    indexPath = soundsLikeTableView.indexPath(for: cell) as NSIndexPath!
                }
            }
        }
        
        let object = soundsLike[indexPath.row] as! PFObject
        let userArray = object["users"] as? NSMutableArray
        
        let query = PFQuery(className: "soundsLike")
        query.getObjectInBackground(withId: object.objectId!) {
            (result, error) -> Void in
            if (result != nil) {
                let resultUsers = result?["users"] as? NSMutableArray
                
                if (userArray?.contains(PFUser.current()?["username"] as! String))! {
                    resultUsers?.remove(PFUser.current()?["username"] as Any)
                    result?["count"] = String(Int((result?["count"] as? String)!)! - 1)
                }
                else {
                    resultUsers?.add(PFUser.current()?["username"] as! String)
                    result?["count"] = String(Int((result?["count"] as? String)!)! + 1)
                }
                
                result?["users"] = resultUsers
                
                result?.saveInBackground {
                    (success, error) -> Void in
                    if (success) {
                        self.soundsLike.removeAllObjects()
                        self.soundsLikeTableView.reloadData()
                        
                        let query = PFQuery(className: "soundsLike")
                        query.whereKey("band", equalTo: self.band!)
                        query.addDescendingOrder("count")
                        query.limit = 5
                        
                        query.findObjectsInBackground {
                            (objects, error) -> Void in
                            if let objects = objects {
                                for object in objects {
                                    self.soundsLike.add(object)
                                }
                            }
                            self.soundsLikeTableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func soundsLikeMoreBtnTouched(_ sender: Any) {
        performSegue(withIdentifier: "moveToSoundsLikeMore", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let mvc: SoundsLikeMoreViewController = segue.destination as! SoundsLikeMoreViewController
        mvc.band = band
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == self.musicianTableView) {
            let cell = musicianTableView.dequeueReusableCell(withIdentifier: "bandMusicianTableViewCell", for: indexPath) as! bandMusicianTableViewCell
            let musician = musicians[indexPath.row] as! PFObject
            cell.name.text = musician["name"] as! String?
            return cell
        }
        
        if (tableView == self.genreTableView) {
            let cell = genreTableView.dequeueReusableCell(withIdentifier: "bandGenreTableViewCell", for: indexPath) as! bandGenreTableViewCell
            let genre = genres[indexPath.row] as! PFObject
            cell.genre.text = genre["genre"] as! String?
            return cell
        }
        
        if (tableView == self.soundsLikeTableView) {
                let cell = soundsLikeTableView.dequeueReusableCell(withIdentifier: "SoundsLikeTableViewCell", for: indexPath) as! SoundsLikeTableViewCell
                let object = soundsLike[indexPath.row] as! PFObject
                
                cell.bandName.text = object["name"] as? String
                cell.count.text = object["count"] as? String
                
                let userArray = object["users"] as? NSMutableArray
                
                if (PFUser.current() != nil) {
                    if (userArray?.contains(PFUser.current()?["username"] as? String! as Any))! {
                        cell.likeSoundsLike.alpha = 0.5
                    }
                    else {
                        cell.likeSoundsLike.alpha = 1
                    }
                    
                    cell.likeSoundsLike.isEnabled = true
                }
                else {
                    cell.likeSoundsLike.alpha = 0.5
                    cell.likeSoundsLike.isEnabled = false
                }
                
                return cell
        }
        
        return UITableViewCell.init()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

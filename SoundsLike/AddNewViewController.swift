//
//  AddNewViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/1/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class AddNewViewController: UIViewController {

    @IBOutlet var band: UIButton!
    @IBOutlet var musician: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        band.layer.borderColor = UIColor.init(red: 103/255, green: 193/255, blue: 166/255, alpha: 1).cgColor
        band.layer.borderWidth = 1
        band.layer.cornerRadius = 5
        
        musician.layer.borderColor = UIColor.init(red: 103/255, green: 193/255, blue: 166/255, alpha: 1).cgColor
        musician.layer.borderWidth = 1
        musician.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func musicianButtonDown(_ sender: Any) {
        musician.backgroundColor = UIColor.init(red: 74/255, green: 196/255, blue: 165/255, alpha: 0.1)
    }
    
    @IBAction func musicianButtonUp(_ sender: Any) {
        musician.backgroundColor = UIColor.clear
    }
    
    @IBAction func bandButtonDown(_ sender: Any) {
        band.backgroundColor = UIColor.init(red: 74/255, green: 196/255, blue: 165/255, alpha: 0.1)
    }
    
    @IBAction func bandButtonUp(_ sender: Any) {
        band.backgroundColor = UIColor.clear
        performSegue(withIdentifier: "moveToAddBand", sender: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

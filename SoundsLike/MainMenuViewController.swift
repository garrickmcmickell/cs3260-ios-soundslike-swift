//
//  MainMenuViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/1/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class MainMenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = UIColor.init(red: 14/255, green: 86/255, blue: 178/255, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addBtnTouched(_ sender: Any) {
        if (PFUser.current() == nil)
        {
            let alert = UIAlertController(title: "Error", message: "You must be logged in to add new items.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            performSegue(withIdentifier: "moveToAddNew", sender: nil)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

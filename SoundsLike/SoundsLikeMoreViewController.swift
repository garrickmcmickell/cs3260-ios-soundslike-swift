//
//  SoundsLikeMoreViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/4/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class SoundsLikeMoreViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var searchBar: UISearchBar!
    var band: PFObject? = nil
    let searchResults: NSMutableArray = []
    @IBOutlet var resultsTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultsTableView.allowsSelection = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reloadResults()
    }
    
    func reloadResults() -> Void {
        searchResults.removeAllObjects()
        resultsTableView.reloadData()
        
        let query = PFQuery(className: "soundsLike")
        query.whereKey("band", equalTo: band!)
        query.addDescendingOrder("count")
        query.limit = 100
        
        query.findObjectsInBackground {
            (objects, error) -> Void in
            if let objects = objects {
                for object in objects {
                    self.searchResults.add(object)
                }
            }
            self.resultsTableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchResults.removeAllObjects()
        resultsTableView.reloadData()
        
        if (searchText != "")
        {
            let query = PFQuery(className: "soundsLike")
            query.whereKey("band", equalTo: band!)
            query.whereKey("searchName", contains: searchBar.text?.lowercased())
            query.addDescendingOrder("count")
            query.limit = 100
            
            query.findObjectsInBackground {
                (objects, error) -> Void in
                if let objects = objects {
                    for object in objects {
                        self.searchResults.add(object)
                    }
                }
                self.resultsTableView.reloadData()
            }
        }
        else {
            reloadResults()
        }
    }
    
    @IBAction func likeSoundsLikeBtnTouched(_ sender: Any) {
        var indexPath: NSIndexPath!
        
        
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? SoundsLikeTableViewCell {
                    indexPath = resultsTableView.indexPath(for: cell) as NSIndexPath!
                }
            }
        }
        
        let object = searchResults[indexPath.row] as! PFObject
        let userArray = object["users"] as? NSMutableArray
        
        let query = PFQuery(className: "soundsLike")
        query.getObjectInBackground(withId: object.objectId!) {
            (result, error) -> Void in
            if (result != nil) {
                let resultUsers = result?["users"] as? NSMutableArray
                
                if (userArray?.contains(PFUser.current()?["username"] as! String))! {
                    resultUsers?.remove(PFUser.current()?["username"] as Any)
                    result?["count"] = String(Int((result?["count"] as? String)!)! - 1)
                }
                else {
                    resultUsers?.add(PFUser.current()?["username"] as! String)
                    result?["count"] = String(Int((result?["count"] as? String)!)! + 1)
                }
                
                result?["users"] = resultUsers
                
                result?.saveInBackground {
                    (success, error) -> Void in
                    if (success) {
                        self.reloadResults()
                    }
                }
            }
        }
    }
    
    @IBAction func addBtnTouched(_ sender: Any) {
        performSegue(withIdentifier: "moveToAddSoundsLike", sender: nil)
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let avc: AddSoundsLikeViewController = segue.destination as! AddSoundsLikeViewController
        avc.band = band
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = resultsTableView.dequeueReusableCell(withIdentifier: "SoundsLikeTableViewCell", for: indexPath) as! SoundsLikeTableViewCell
        let object = searchResults[indexPath.row] as! PFObject
        cell.bandName.text = object["name"] as? String
        cell.count.text = object["count"] as? String
        
        let userArray = object["users"] as? NSMutableArray
        
        if (PFUser.current() != nil) {
            if (userArray?.contains(PFUser.current()?["username"] as? String! as Any))! {
                cell.likeSoundsLike.alpha = 0.5
            }
            else {
                cell.likeSoundsLike.alpha = 1
            }
            cell.likeSoundsLike.isEnabled = true
            
        }
        else {
            cell.likeSoundsLike.alpha = 0.5
            cell.likeSoundsLike.isEnabled = false
        }
    
        return cell
    }

}

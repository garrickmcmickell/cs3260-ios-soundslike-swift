//
//  ProfileViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 11/19/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class ProfileViewController: UIViewController {

    @IBOutlet var navItem: UINavigationItem!
    @IBOutlet var username: UILabel!
    @IBOutlet var picture: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navItem.title = PFUser.current()?.object(forKey: "username") as? String
        username.text = PFUser.current()?.object(forKey: "username") as? String
        if let profilePicture = PFUser.current()?.object(forKey: "image") as? PFFile {
            profilePicture.getDataInBackground {
                (data, error) -> Void in
                if (data != nil) {
                    self.picture.setImage(UIImage(data: data!), for: UIControlState.normal)
                }
            }
        }
    }

    @IBAction func logOutBtnTouched(_ sender: Any) {
        PFUser.logOut()
        navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func editBtnTouched(_ sender: Any) {
        performSegue(withIdentifier: "moveToEditProfile", sender: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

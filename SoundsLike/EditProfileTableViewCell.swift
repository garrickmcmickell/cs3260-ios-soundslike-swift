//
//  EditProfileTableViewCell.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class EditProfileTableViewCell: UITableViewCell {

    @IBOutlet var value: UITextField!
    @IBOutlet var type: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  LoginViewController.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 11/19/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController {

    @IBOutlet var password: UITextField!
    @IBOutlet var username: UITextField!
    @IBOutlet var signIn: UIButton!
    @IBOutlet var signUp: UIButton!
    @IBOutlet var loading: UIActivityIndicatorView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        signIn.layer.borderColor = UIColor.init(red: 103/255, green: 193/255, blue: 166/255, alpha: 1).cgColor
        signIn.layer.borderWidth = 1
        signIn.layer.cornerRadius = 5
        
        signUp.layer.borderColor = UIColor.init(red: 103/255, green: 193/255, blue: 166/255, alpha: 1).cgColor
        signUp.layer.borderWidth = 1
        signUp.layer.cornerRadius = 5
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = UIColor.init(red: 14/255, green: 86/255, blue: 178/255, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        username.text = ""
        password.text = ""
        
        if (PFUser.current() != nil) {
            performSegue(withIdentifier: "moveToProfile", sender: nil)
        }
    }
    
    @IBAction func signInBtnTouched(_ sender: Any) {
        signIn.backgroundColor = UIColor.clear
        loading.startAnimating()
        view.alpha = 0.5
        PFUser.logInWithUsername(inBackground: username.text!, password: password.text!, block: {(success, error) in
            if(success != nil){
                self.performSegue(withIdentifier: "moveToProfile", sender: nil)
                self.loading.stopAnimating()
                self.view.alpha = 1
            }
            else{
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription ?? String(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.loading.stopAnimating()
                self.view.alpha = 1
            }
        })
    }
    
    @IBAction func signInBtnReleased(_ sender: Any) {
        signIn.backgroundColor = UIColor.init(red: 74/255, green: 196/255, blue: 165/255, alpha: 0.1)
    }
    
    @IBAction func signUpBtnTouched(_ sender: Any) {
        signUp.backgroundColor = UIColor.clear
        navigationController?.isNavigationBarHidden = false
        performSegue(withIdentifier: "moveToSignUp", sender: nil)
    }
    
    @IBAction func signUpTouchDown(_ sender: Any) {
        signUp.backgroundColor = UIColor.init(red: 74/255, green: 196/255, blue: 165/255, alpha: 0.1)
    }


}

//
//  AddBandGenreTableViewCell.swift
//  SoundsLike
//
//  Created by Garrick McMickell on 12/1/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class AddBandGenreTableViewCell: UITableViewCell {

    @IBOutlet var genreTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
